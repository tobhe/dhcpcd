Source: dhcpcd
Section: net
Priority: optional
Homepage: https://roy.marples.name/projects/dhcpcd
Maintainer: Martin-Éric Racine <martin-eric.racine@iki.fi>
Build-Depends: debhelper-compat (= 13),
               libudev-dev [linux-any],
               pkg-config
Rules-Requires-Root: no
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/debian/dhcpcd
Vcs-Git: https://salsa.debian.org/debian/dhcpcd.git

Package: dhcpcd-base
Architecture: any
Provides: dhcp-client
Replaces: dhcpcd5 (<< 9.4.1-2)
Breaks: dhcpcd5 (<< 9.4.1-2)
Depends: adduser,
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: wpasupplicant
Suggests: resolvconf | openresolv | systemd-resolved
Description: DHCPv4 and DHCPv6 dual-stack client (binaries and exit hooks)
 dhcpcd provides seamless IPv4 and IPv6 auto-configuration:
  * DHCPv4 client
  * DHCPv6 client with Prefix Delegation (PD) support
  * IPv4 LL support (ZeroConf)
  * IPv6 SLAAC support
  * ARP address conflict resolution
  * ARP ping profiles
  * Wireless SSID profiles
 .
 This package provides the binaries, exit hooks and manual pages.
 .
 It offers a dual-stack substitute for ISC DHCP Client (dhclient) on systems
 where interfaces are configured by ifupdown via </etc/network/interfaces>
 using the DHCP method.
 .
 The init.d script and systemd service required for systems without ifupdown
 are packaged separately as dhcpcd.
 .
 Since Debian 12 (Bookworm), dhcpcd uses Predictable Network Interface Names
 on Linux ports. See NEWS.Debian for more details.

Package: dhcpcd
Architecture: all
Depends: dhcpcd-base,
         sysvinit-utils (>= 3.05-4~) | lsb-base (>= 3.0-6),
         ${misc:Depends},
         ${shlibs:Depends}
Provides: dhcpcd
Replaces: dhcpcd5 (<< 9.4.1-12)
Breaks: dhcpcd5 (<< 9.4.1-12)
Suggests: dhcpcd-gtk
Description: DHCPv4 and DHCPv6 dual-stack client (init.d script and systemd unit)
 dhcpcd provides seamless IPv4 and IPv6 auto-configuration.
 .
 This package provides the optional init.d script and systemd service.
 .
 It should not be installed on systems where interfaces are configured
 by ifupdown via </etc/network/interfaces> using the DHCP method.

Package: dhcpcd5
Section: oldlibs
Architecture: all
Depends: dhcpcd,
         ${misc:Depends},
         ${shlibs:Depends}
Description: DHCPv4 and DHCPv6 dual-stack client (dummy transitional package)
 This package depends on dhcpcd. It can safely be removed after upgrades.
